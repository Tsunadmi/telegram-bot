import requests
from messages import MESSAGES
from coin import define_coin


def get_info_statistic(cion):
    try:
        data_for_search = cion.lower()
        data_for_output = cion.upper()
        req = requests.get(url="https://yobit.net/api/3/trades/" + data_for_search + "_usd")
        response = req.json()
        if (data_for_search + "_usd") in define_coin():
            total_trade_ask = 0
            total_trade_bit = 0
            for item in response[data_for_search + '_usd']:
                if item["type"] == "ask":
                    total_trade_ask += item["price"] * item["amount"]
                else:
                    total_trade_bit += item["price"] * item["amount"]
            info = f"[-] TOTAL {data_for_output} SELL: {round(total_trade_ask, 2)}$\n[+] TOTAL {data_for_output} BUY: {round(total_trade_bit, 2)}$"
            return info
        else:
            return MESSAGES['error_of_coin']
    except:
        return MESSAGES['error_of_coin']
