message_info = "Welcome to CryptoCurrency_ Bot.\nThis bot was created as first real project in IT industry. This bot give you information about Cryptocurrency and others information.\n\n\U00002714IF you push \U0001FA99 Coin and enter Coin-name. (example : BTC, SHIB, ETH etc.) you will get price of sell and buy of chosen coin.\n\U00002714IF you push \U0000303d Coin statistic and enter Coin-name (example : BTC, SHIB, ETH etc.) you will get total price of sell and buy for last 150 orders in chosen coin.\n\U00002714IF you push \U0001F4E7 Send email and enter your email address, bot will send the link with safety cryptocurrency exchange platform which we recommend.\n\U00002714IF you push \U0001F4B5 Currency, you can find 6 the most world popular currency.\n\U00002714IF you push \U0001F327Weather and enter City-name, you will get all information about weather in a chosen city. "

subject = "Cryptocurrency exchange platform!"

subject_success = "The message was sent successfully!"

subject_exept = "Check your login please!"

error_of_coin = "This coin is not in database!\nYou need enter → (example: BTC, ETH ect.)."

enter_mail = "Enter your email please:"

enter_coin = "Enter your coin-name please → (example: BTC, ETH ect.):"

hello = "Hello {0.first_name},\n\nWelcome to cryptocurrency world my friend, enjoy that bot.\n\nChose the options in tab menu below:"

chose = "You can chose officially courses of currency below:"

visit_website = 'Push the button for visit website.'

else_not_correct = 'Command is not correct, try one more please.'

mail = 'Hello my friend.\nIn link below you can find safety cryptocurrency exchange platform which we recommend:\nhttps://www.coinbase.com/\nBest Regards,\nCryptoCurrency_ Bot'

successful_payment = 'All good!'

add_option = "Additional options don't available now, please try it later."

NOTIFICATION = "THIS WAS TEXT NOTIFICATION!"

payment = 'Payment was successful. Thanks for payment!'

transaction_error = 'Connection or transaction error, try to pay again in a few minutes.'

weather = "Enter city-name please:"

cc = 'Welcome to Crypto-Currency menu.'

other = 'Welcome to Others menu.'

currency = "\U00002B07This is yours result\U00002B07"

chose_coin = 'You can chose coin in table below:'

MESSAGES = {
    'info': message_info,
    'subject': subject,
    'subject_success': subject_success,
    'subject_exept': subject_exept,
    'error_of_coin': error_of_coin,
    'enter_mail': enter_mail,
    'enter_coin': enter_coin,
    'hello': hello,
    'chose': chose,
    'visit_website': visit_website,
    'else_not_correct': else_not_correct,
    'mail': mail,
    'successful_payment': successful_payment,
    'add_option': add_option,
    'NOTIFICATION': NOTIFICATION,
    'payment': payment,
    'transaction_error': transaction_error,
    'weather': weather,
    'cc': cc,
    'other': other,
    'currency': currency,
    'chose_coin': chose_coin
}
