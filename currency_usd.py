import requests
from bs4 import BeautifulSoup


def get_currency_usd(currency_define):
    currency = "https://www.google.com/search?q=kurz+dolar+" + currency_define
    headers = {'Accept': "*/*",
               "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0"}
    page = requests.get(currency, headers=headers)
    soup = BeautifulSoup(page.content, 'lxml')
    currency_parser = soup.find_all(class_="DFlfde SwHCTb")
    final_currency = float(str(currency_parser[0].text).replace(',', '.'))
    return f' 1$ → {final_currency}'
