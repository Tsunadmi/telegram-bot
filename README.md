# Telegram Bot

Crypto Telegram Bot (My first real project)

## Installation

You need install python 3.9:
```
pip install python
```
## Virtual environment

In terminal in project folder you should activate virtual environment:

```
venv/Scripts/activate
```

## Add important libraries

```
pip install aiogram
pip install aiosignal
pip install asgiref
pip install attrs
pip install Babel
pip install beautifulsoup4
pip install bs4
pip install certifi
pip install charset-normalizer
pip install frozenlist
pip install lxml
pip install multidict
pip install pyTelegramBotAPI
pip install python-dateutil
pip install pytz
pip install requests
pip install soupsieve
pip install sqlparse
pip install telebot
pip install tzdata
pip install urllib3
```

## Getting started

Start of bot:
```
python My_bot.py
```
## Technologies used

- Python
- JetBrains
- Cryptocurrency API
- Weather API
- Currency API
- Parsing data
- Telegram Bot
- JSON

## Data

- [ ] [Web with Crypto API](https://www.yobit.net/en)
- [ ] [Web with Weather API](https://openweathermap.org/api)
- [ ] [Web with Currency information :) ](https://www.google.com/)
- [ ] [Hosting](https://zomro.com/?from=246874)

## Description

This bot gives you information about Cryptocurrency and other miscellaneous information.

You can enter coin name and you chose option which you need, for example:

- [coin.py]

```
def define_coin()
```
This function define coin if exist.

```
def coin_data(cion)
def coin_data_sell(cion)
def coin_data_buy(cion)

```
Those functions parsing data of sell and buy of the chosen coin.

- [currency_usd.py]

```
def get_currency_usd(currency_define)
```
This function gives information about 6 most popular currency rates to usd.

- [statistic_coin.py]

```
def get_info_statistic(cion)
```
This function parsing data of total price of sell and buy of the last 150 orders of the chosen coin.

- [weather.py]

```
def get_weather(city, token_weather):
```
This function parsing data about weather in the chosen city.

Etc :)
	

## Author

This app was done by Dmitry Tsunaev.

- [ ] [LinkedIn](http://linkedin.com/in/dmitry-tsunaev-530006aa)

