import types
import telebot
from auth_data import token, PAYMENTS_TOKEN, token_weather
from telebot import types
from telebot.types import LabeledPrice, ShippingOption
from messages import MESSAGES
from weather import get_weather
from send_email import send_email
from currency_usd import get_currency_usd
from statistic_coin import get_info_statistic
from coin import coin_data

PRICES = [
    LabeledPrice(label='Product_basic', amount=40000),
    LabeledPrice(label='Product_DLC', amount=5000)]

shipping_options = [
    ShippingOption(id='additional_option', title='First additional option ').add_price(LabeledPrice('First', 2500)),
    ShippingOption(id='additional_option_2', title='Second additional option').add_price(LabeledPrice('Pickup', 1500)),
    ShippingOption(id='additional_option_3', title='Vip additional option').add_price(LabeledPrice('Pickup', 10000))]


def telegram_bot(token):
    bot = telebot.TeleBot(token)

    @bot.message_handler(commands=["start"])
    def start_bot(message):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
        item = []
        list_button_name = ['\U0001F911Crypto-Currency', 'Others']
        for name in list_button_name:
            value = types.KeyboardButton(name)
            item.append(value)
        markup.add(*item)
        bot.send_message(message.chat.id, MESSAGES['hello'].format(
            message.from_user, bot.get_me()), parse_mode='html', reply_markup=markup)

    @bot.message_handler(content_types=['text'])
    def start(message):
        if message.text == '\U0001F911Crypto-Currency':
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            item = []
            list_button_name = ['\U0001FA99 Find Coin', '\U0000303d Coin statistic', '\U0001F4B5 Currency',
                                '\U0001F9FE Coin Table', '\U0001F519Back']
            for name in list_button_name:
                value = types.KeyboardButton(name)
                item.append(value)
            markup.add(*item)
            bot.send_message(message.chat.id, MESSAGES['cc'], reply_markup=markup)
        elif message.text == 'Others':
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            item = []
            list_button_name = ['\U0001F4E7 Send email', '\U0001F4E1 Website', '\U0001F4B3 Payment',
                                '\U00002753 About bot', '\U0001F327Weather', '\U0001F519Back']
            for name in list_button_name:
                value = types.KeyboardButton(name)
                item.append(value)
            markup.add(*item)
            bot.send_message(message.chat.id, MESSAGES['other'], reply_markup=markup)
        elif message.text == '\U0001F519Back':
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            item = []
            list_button_name = ['\U0001F911Crypto-Currency', 'Others']
            for name in list_button_name:
                i = types.KeyboardButton(name)
                item.append(i)
            markup.add(*item)
            bot.send_message(message.chat.id, '\U0001F519Back', reply_markup=markup)
        elif message.text == '\U0001F4E7 Send email':
            bot.send_message(message.from_user.id, MESSAGES['enter_mail']);
            bot.register_next_step_handler(message, get_mail);
        elif message.text == '\U0001FA99 Find Coin':
            bot.send_message(message.from_user.id, MESSAGES['enter_coin'])
            bot.register_next_step_handler(message, get_coin)
        elif message.text == '\U0000303d Coin statistic':
            bot.send_message(message.from_user.id, MESSAGES['enter_coin'])
            bot.register_next_step_handler(message, get_statistic_coin)
        elif message.text == '\U0001F4B5 Currency':
            markup = types.InlineKeyboardMarkup(row_width=3)
            item = []
            list_currency = ['USD/EUR', 'USD/CNY', 'USD/CZK', 'USD/RUB', 'USD/GBP', 'USD/JPY', 'Done!']
            for name in list_currency:
                value = types.InlineKeyboardButton(name, callback_data=name)
                item.append(value)
            markup.add(*item)
            bot.send_message(message.from_user.id, MESSAGES['chose'], reply_markup=markup)
        elif message.text == '\U0001F9FE Coin Table':
            markup = types.InlineKeyboardMarkup(row_width=3)
            item = []
            list_currency = ['BTC', 'SHIB', 'XRP', 'ETH', 'USDT', 'DOGE', 'Done!']
            for name in list_currency:
                value = types.InlineKeyboardButton(name, callback_data=name)
                item.append(value)
            markup.add(*item)
            bot.send_message(message.from_user.id, MESSAGES['chose_coin'], reply_markup=markup)
        elif message.text == '\U00002753 About bot':
            bot.send_message(message.from_user.id, MESSAGES['info'])
        elif message.text == '\U0001F4E1 Website':
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton('Go to website', url='https://www.yobit.net/en'))
            bot.send_message(message.chat.id, MESSAGES['visit_website'], reply_markup=markup)
        elif message.text == '\U0001F327Weather':
            bot.send_message(message.from_user.id, MESSAGES['weather'])
            bot.register_next_step_handler(message, get_weather_bot)
        elif message.text == '\U0001F4B3 Payment':
            bot.send_invoice(message.chat.id,
                             title='Main product',
                             description='Special for you',
                             provider_token=PAYMENTS_TOKEN,
                             currency='RUB',
                             prices=PRICES,
                             need_email=True,
                             need_phone_number=True,
                             is_flexible=True,
                             invoice_payload='Invoice-payload',
                             start_parameter='Start'
                             )
        else:
            bot.send_message(message.from_user.id, MESSAGES['else_not_correct']);

    def get_coin(message):
        bot.send_message(message.from_user.id, coin_data(cion=message.text))

    def get_mail(message):
        bot.send_message(message.from_user.id, send_email(message=MESSAGES['mail'], mail=message.text));

    def get_statistic_coin(message):
        bot.send_message(message.from_user.id, get_info_statistic(cion=message.text));

    def get_weather_bot(message):
        bot.send_message(message.from_user.id, get_weather(city=message.text, token_weather=token_weather))

    @bot.callback_query_handler(func=lambda call: True)
    def callback_inline(call):
        try:
            if call.message:
                if call.data == 'USD/EUR':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='euro') + '€')
                elif call.data == 'USD/CNY':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='cny') + '¥')
                elif call.data == 'USD/CZK':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='czk') + 'Kč')
                elif call.data == 'USD/RUB':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='rub') + '₽')
                elif call.data == 'USD/GBP':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='gbp') + '£')
                elif call.data == 'USD/JPY':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='jpy') + '¥')
                elif call.data == 'BTC':
                    bot.send_message(call.message.chat.id, coin_data(cion='btc'))
                elif call.data == 'SHIB':
                    bot.send_message(call.message.chat.id, coin_data(cion='shib'))
                elif call.data == 'XRP':
                    bot.send_message(call.message.chat.id, coin_data(cion='xrp'))
                elif call.data == 'ETH':
                    bot.send_message(call.message.chat.id, coin_data(cion='eth'))
                elif call.data == 'USDT':
                    bot.send_message(call.message.chat.id, coin_data(cion='usdt'))
                elif call.data == 'DOGE':
                    bot.send_message(call.message.chat.id, coin_data(cion='doge'))
                elif  call.data == 'Done!':
                    bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                          text=MESSAGES["currency"],
                                          reply_markup=None)
                bot.answer_callback_query(callback_query_id=call.id, show_alert=False,
                                          text=MESSAGES["NOTIFICATION"])

        except Exception as e:
            print(repr(e))

    @bot.shipping_query_handler(func=lambda query: True)
    def shipping(shipping_query):
        bot.answer_shipping_query(shipping_query.id, ok=True, shipping_options=shipping_options,
                                  error_message=MESSAGES['add_option'])

    @bot.pre_checkout_query_handler(func=lambda query: True)
    def checkout(pre_checkout_query):
        bot.answer_pre_checkout_query(pre_checkout_query.id, ok=True,
                                      error_message=MESSAGES["transaction_error"])

    @bot.message_handler(content_types=['successful_payment'])
    def got_payment(message):
        bot.send_message(message.chat.id, MESSAGES['payment'])

    bot.polling()


if __name__ == "__main__":
    telegram_bot(token)
    # get_info()
