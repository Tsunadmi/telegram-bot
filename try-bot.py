import types
import requests
import json
import telebot
import smtplib
from auth_data import token, PAYMENTS_TOKEN, token_weather
from telebot import types
from telebot.types import LabeledPrice, ShippingOption
from email.mime.text import MIMEText
from bs4 import BeautifulSoup
from messages import MESSAGES
from datetime import datetime
from weather import get_weather

PRICES = [
    LabeledPrice(label='Product_basic', amount=40000),
    LabeledPrice(label='Product_DLC', amount=5000)]

shipping_options = [
    ShippingOption(id='additional_option', title='First additional option ').add_price(LabeledPrice('First', 2500)),
    ShippingOption(id='additional_option_2', title='Second additional option').add_price(LabeledPrice('Pickup', 1500)),
    ShippingOption(id='additional_option_3', title='Vip additional option').add_price(LabeledPrice('Pickup', 10000))]

# def get_weather(city, token_weather):
#     try:
#         response = requests.get(f"https://api.openweathermap.org/data/2.5/weather?q={city}&appid={token_weather}&units=metric")
#         data=response.json()
#         city_name=data['name']
#         temp=data["main"]["temp"]
#         humidity = data["main"]["humidity"]
#         wind = data["wind"]["speed"]
#         clouds = data["clouds"]["all"]
#         sunrise = datetime.fromtimestamp(data["sys"]["sunrise"])
#         sunset = datetime.fromtimestamp(data["sys"]["sunset"])
#         return (f'City name: {city_name}\nTemperature: {temp} °C\nHumidity: {humidity} %\nWind: {wind} km/h\nClouds: {clouds} %\nSunrise time: {sunrise}'
#                 f'\nSunset time: {sunset}')
#     except Exception as ex:
#         return f'{ex} -> Mistake, try one more!'

def get_info():
    response = requests.get(url="https://yobit.net/api/3/info")
    with open ("info.txt", "w") as file:
        file.write(response.text)
    return response.text

def send_email(message, mail):
    my_mail = 'tsunadmi@gmail.com'
    password = "ifyvjjnfczirxeuq"
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.starttls()
    try:
        server.login(my_mail, password)
        msg = MIMEText(message)
        msg["Subject"] = MESSAGES['subject']
        server.sendmail(my_mail,mail, msg.as_string())
        return MESSAGES ['subject_success']
    except Exception as _ex:
        return MESSAGES['subject_exept']

def get_currency_usd(currency_define):
    currency = "https://www.google.com/search?q=kurz+dolar+"+currency_define
    headers = {'Accept': "*/*",
               "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0"}
    page = requests.get(currency, headers=headers)
    soup = BeautifulSoup(page.content, 'lxml')
    currency_parser = soup.find_all(class_="DFlfde SwHCTb")
    final_currency = float(str(currency_parser[0].text).replace(',','.'))
    return f' 1$ → {final_currency}'

def define_coin():
    with open('info.txt') as json_file:
        data = json.load(json_file)
        all_coins = []
        for item in data['pairs']:
            all_coins.append(item)
        json_file.close()
    return all_coins

def get_info_statistic(cion):
    try:
        data_for_search = cion.lower()
        data_for_output = cion.upper()
        req = requests.get(url="https://yobit.net/api/3/trades/"+ data_for_search +"_usd")
        response = req.json()
        if (data_for_search + "_usd") in define_coin():
            total_trade_ask = 0
            total_trade_bit = 0
            for item in response[data_for_search +'_usd']:
                if item["type"] == "ask":
                    total_trade_ask += item["price"]* item["amount"]
                else:
                    total_trade_bit += item["price"]* item["amount"]
            info = f"[-] TOTAL {data_for_output} SELL: {round(total_trade_ask,2)}$\n[+] TOTAL {data_for_output} BUY: {round(total_trade_bit,2)}$"
            return info
        else:
            return MESSAGES['error_of_coin']
    except:
        return MESSAGES['error_of_coin']

def coin_data(cion):
    try:
        data_for_search = cion.lower()
        data_for_output = cion.upper()
        req = requests.get(url="https://yobit.net/api/3/ticker/"+ data_for_search + "_usd")
        response = req.json()
        if (data_for_search + "_usd") in define_coin():
            sell_price = response[data_for_search +'_usd']['sell']
            buy_price = response[data_for_search +'_usd']['buy']
            if sell_price > 0.001 and buy_price > 0.001:
                sell_price_short = round(sell_price, 2)
                buy_price_short = round(buy_price, 2)
                return (f"{datetime.now().strftime('%Y-%m-%d %H:%M')}\nSell {data_for_output} price: {sell_price_short}$\nBuy {data_for_output} price: {buy_price_short}$")
            else:
                return (f"{datetime.now().strftime('%Y-%m-%d %H:%M')}\nSell {data_for_output} price: {sell_price}$\nBuy {data_for_output} price: {buy_price}$")
        else:
            return MESSAGES['error_of_coin']
    except:
        return MESSAGES['error_of_coin']

def coin_data_sell(cion):
    data_for_search = cion.lower()
    req = requests.get(url="https://yobit.net/api/3/ticker/"+ data_for_search + "_usd")
    response = req.json()
    if (data_for_search + "_usd") in define_coin():
        sell_price = response[data_for_search +'_usd']['sell']
    return sell_price

def coin_data_buy(cion):
    data_for_search = cion.lower()
    req = requests.get(url="https://yobit.net/api/3/ticker/"+ data_for_search + "_usd")
    response = req.json()
    if (data_for_search + "_usd") in define_coin():
        buy_price = response[data_for_search +'_usd']['buy']
    return buy_price

def telegram_bot(token):
    bot = telebot.TeleBot(token)

    @bot.message_handler(commands=["start"])
    def start_bot(message):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
        item = []
        list_button_name = ['\U0001F911Crypto-Currency', 'Others']
        for name in list_button_name:
            i = types.KeyboardButton(name)
            item.append(i)
        markup.add(*item)
        bot.send_message(message.chat.id, MESSAGES['hello'].format(
                             message.from_user, bot.get_me()), parse_mode='html', reply_markup=markup)

    @bot.message_handler(content_types=['text'])
    def start(message):
        if message.text == '\U0001F911Crypto-Currency':
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            item = []
            list_button_name = ['\U0001FA99 Coin', '\U0000303d Coin statistic', '\U0001F4B5 Currency', '\U0001F519Back']
            for name in list_button_name:
                i = types.KeyboardButton(name)
                item.append(i)
            markup.add(*item)
            bot.send_message(message.chat.id, MESSAGES['cc'], reply_markup=markup)
        elif message.text == 'Others':
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            item = []
            list_button_name = ['\U0001F4E7 Send email', '\U0001F4E1 Website', '\U0001F4B3 Payment', '\U00002753 About bot', '\U0001F327Weather', '\U0001F519Back']
            for name in list_button_name:
                i = types.KeyboardButton(name)
                item.append(i)
            markup.add(*item)
            bot.send_message(message.chat.id, MESSAGES['other'], reply_markup=markup)
        elif message.text == '\U0001F519Back':
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            item = []
            list_button_name = ['\U0001F911Crypto-Currency', 'Others']
            for name in list_button_name:
                i = types.KeyboardButton(name)
                item.append(i)
            markup.add(*item)
            bot.send_message(message.chat.id, '\U0001F519Back', reply_markup=markup)
        elif message.text == '\U0001F4E7 Send email':
            bot.send_message(message.from_user.id, MESSAGES['enter_mail']);
            bot.register_next_step_handler(message, get_mail);
        elif message.text == '\U0001FA99 Coin':
            markup = types.InlineKeyboardMarkup(row_width=3)
            item = []
            list_currency = ['BTC', 'SHIB', 'XRP', 'ETH', 'USDT', 'CHOOSE OTHER']
            for name in list_currency:
                i = types.InlineKeyboardButton(name, callback_data=name)
                item.append(i)
            markup.add(*item)
            bot.send_message(message.from_user.id, MESSAGES['enter_coin'], reply_markup=markup)
            bot.register_next_step_handler(message, get_coin)
        elif message.text == '\U0000303d Coin statistic':
            bot.send_message(message.from_user.id, MESSAGES['enter_coin'])
            bot.register_next_step_handler(message, get_statistic_coin)
        elif message.text == '\U0001F4B5 Currency':
            markup = types.InlineKeyboardMarkup(row_width=3)
            item =[]
            list_currency = ['USD/EUR', 'USD/CNY', 'USD/CZK', 'USD/RUB', 'USD/GBP', 'USD/JPY']
            for name in list_currency:
                i = types.InlineKeyboardButton(name, callback_data=name)
                item.append(i)
            markup.add(*item)
            bot.send_message(message.from_user.id, MESSAGES['chose'], reply_markup=markup)
        elif message.text == '\U00002753 About bot':
            bot.send_message(message.from_user.id, MESSAGES['info'])
        elif message.text == '\U0001F4E1 Website':
            markup = types.InlineKeyboardMarkup()
            markup.add(types.InlineKeyboardButton('Go to website', url='https://www.yobit.net/en'))
            bot.send_message(message.chat.id, MESSAGES['visit_website'] , reply_markup=markup)
        elif message.text =='\U0001F327Weather':
            bot.send_message(message.from_user.id, MESSAGES['weather'])
            bot.register_next_step_handler(message, get_weather_bot)
        elif message.text =='\U0001F4B3 Payment':
            bot.send_invoice(message.chat.id,
                             title='Main product',
                             description='Special for you',
                             provider_token=PAYMENTS_TOKEN,
                             currency='RUB',
                             prices=PRICES,
                             need_email=True,
                             need_phone_number=True,
                             is_flexible=True,
                             invoice_payload= 'blabla',
                             start_parameter='123'
                             )
        else:
            bot.send_message(message.from_user.id, MESSAGES['else_not_correct']);

    def get_coin(message):
        bot.send_message(message.from_user.id, coin_data(cion=message.text))

    def get_mail(message):
        bot.send_message(message.from_user.id, send_email(message= MESSAGES['mail'] , mail=message.text));

    def get_statistic_coin(message):
        bot.send_message(message.from_user.id, get_info_statistic(cion=message.text));

    def get_weather_bot(message):
        bot.send_message(message.from_user.id, get_weather(city=message.text, token_weather=token_weather))

    @bot.callback_query_handler(func=lambda call: True)
    def callback_inline(call):
        try:
            if call.message:
                if call.data == 'USD/EUR':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='euro') + '€')
                elif call.data == 'USD/CNY':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='cny')+ '¥')
                elif call.data == 'USD/CZK':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='czk') + 'Kč')
                elif call.data == 'USD/RUB':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='rub') + '₽')
                elif call.data == 'USD/GBP':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='gbp') + '£')
                elif call.data == 'USD/JPY':
                    bot.send_message(call.message.chat.id, get_currency_usd(currency_define='jpy') + '¥')
                elif call.data == 'BTC':
                    bot.send_message(call.message.chat.id, coin_data(cion='btc'))
                elif call.data == 'SHIB':
                    bot.send_message(call.message.chat.id, coin_data(cion='SHIB'))
                elif call.data == 'ETH':
                    bot.send_message(call.message.chat.id, coin_data(cion='ETH'))
                elif call.data == 'USDT':
                    bot.send_message(call.message.chat.id, coin_data(cion='USDT'))
                elif call.data == 'XRP':
                    bot.send_message(call.message.chat.id, coin_data(cion='XRP'))

                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="\U00002B07 This is yours result",
                                      reply_markup=None)
                bot.answer_callback_query(callback_query_id=call.id, show_alert=False,
                                          text=MESSAGES["NOTIFICATION"])

        except Exception as e:
            print(repr(e))

    @bot.shipping_query_handler(func=lambda query: True)
    def shipping(shipping_query):
        bot.answer_shipping_query(shipping_query.id, ok=True,  shipping_options=shipping_options,
                              error_message=MESSAGES['add_option'])

    @bot.pre_checkout_query_handler(func=lambda query: True)
    def checkout(pre_checkout_query):
        bot.answer_pre_checkout_query(pre_checkout_query.id, ok=True,
                                      error_message=MESSAGES["transaction_error"])

    @bot.message_handler(content_types=['successful_payment'])
    def got_payment(message):
        bot.send_message(message.chat.id, MESSAGES['payment'])

    bot.polling()

if __name__ == "__main__":
    telegram_bot(token)
    # get_info()

    # bot.send_message(message.from_user.id, MESSAGES['enter_coin'])
    # bot.register_next_step_handler(message, get_coin)