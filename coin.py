import requests
import json
from datetime import datetime
from messages import MESSAGES


def define_coin():
    with open('info.txt') as json_file:
        data = json.load(json_file)
        all_coins = []
        for item in data['pairs']:
            all_coins.append(item)
        json_file.close()
    return all_coins


def coin_data(cion):
    try:
        data_for_search = cion.lower()
        data_for_output = cion.upper()
        req = requests.get(url="https://yobit.net/api/3/ticker/" + data_for_search + "_usd")
        response = req.json()
        if (data_for_search + "_usd") in define_coin():
            sell_price = response[data_for_search + '_usd']['sell']
            buy_price = response[data_for_search + '_usd']['buy']
            if sell_price > 0.001 and buy_price > 0.001:
                sell_price_short = round(sell_price, 2)
                buy_price_short = round(buy_price, 2)
                return (
                    f"{datetime.now().strftime('%Y-%m-%d %H:%M')}\nSell {data_for_output} price: {sell_price_short}$\nBuy {data_for_output} price: {buy_price_short}$")
            else:
                return (
                    f"{datetime.now().strftime('%Y-%m-%d %H:%M')}\nSell {data_for_output} price: {sell_price}$\nBuy {data_for_output} price: {buy_price}$")
        else:
            return MESSAGES['error_of_coin']
    except:
        return MESSAGES['error_of_coin']


def coin_data_sell(cion):
    data_for_search = cion.lower()
    req = requests.get(url="https://yobit.net/api/3/ticker/" + data_for_search + "_usd")
    response = req.json()
    if (data_for_search + "_usd") in define_coin():
        sell_price = response[data_for_search + '_usd']['sell']
    return sell_price


def coin_data_buy(cion):
    data_for_search = cion.lower()
    req = requests.get(url="https://yobit.net/api/3/ticker/" + data_for_search + "_usd")
    response = req.json()
    if (data_for_search + "_usd") in define_coin():
        buy_price = response[data_for_search + '_usd']['buy']
    return buy_price
