import requests
import datetime


def get_weather(city, token_weather):
    try:
        response = requests.get(
            f"https://api.openweathermap.org/data/2.5/weather?q={city}&appid={token_weather}&units=metric")
        data = response.json()
        city_name = data['name']
        temp = data["main"]["temp"]
        humidity = data["main"]["humidity"]
        wind = data["wind"]["speed"]
        clouds = data["clouds"]["all"]
        sunrise = datetime.datetime.fromtimestamp(data["sys"]["sunrise"])
        sunset = datetime.datetime.fromtimestamp(data["sys"]["sunset"])
        return (
            f'City name: {city_name}\nTemperature: {temp} °C\nHumidity: {humidity} %\nWind: {wind} km/h\nClouds: {clouds} %\nSunrise time: {sunrise}'
            f'\nSunset time: {sunset}')
    except Exception as ex:
        return f'{ex} -> Mistake, try one more!'
